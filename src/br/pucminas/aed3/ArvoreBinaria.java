package br.pucminas.aed3;

class No{
	public byte[] endereco;
	public byte[] id;
	public byte[] esq;
	public byte[] dir;
	public byte lapide;

	
	public No(byte lapide,byte[] endereco, byte[] id, byte[] esq, byte[] dir) {
		this.endereco = endereco;
		this.id = id;
		this.esq = esq;
		this.dir = dir;
		this.lapide = lapide;
	}
	
	public No(byte[] endereco, byte[] id) {
		this.endereco = endereco;
		this.id = id;
		this.esq = this.dir = null;
		
	}
	
	public No(byte[] no){
		lapide = no[0];
		endereco = new byte[]{no[1],no[2],no[3],no[4]};
		id = new byte[]{no[5],no[6],no[7],no[8]};
		esq = new byte[]{no[9],no[10],no[11],no[12]};
		dir = new byte[]{no[13],no[14],no[15],no[16]};
	}
	
	public void mostrar(){
		System.out.println(lapide);
		System.out.println(endereco[0]+""+endereco[1]+""+endereco[2]+""+endereco[3]+"");
		System.out.println(id[0]+""+id[1]+""+id[2]+""+id[3]+"");
		System.out.println(esq[0]+""+esq[1]+""+esq[2]+""+esq[3]+"");
		System.out.println(dir[0]+""+dir[1]+""+dir[2]+""+dir[3]+"");
	}
}

public class ArvoreBinaria{
	public No raiz;
	private Database db;
	public ArvoreBinaria(){
		db = new Database("rw", "PerguntasIndex.txt");
		byte[] b = db.leBytes(4);

		if(byteArrayToInt(b)!=0){
			
			db.ponteiro(byteArrayToInt(b));
			b = db.leBytes(17);

			raiz = new No(b);
		}else{
			raiz = null;
		
			db.ponteiro(db.getTamArquivo());
			db.escreveByte(0);
			db.escreveByte(0);
			db.escreveByte(0);
			db.escreveByte(4);
			
		}
		db.fechaDB();

	}
	
	public No inserir(Pergunta p){
		db = new Database("rw", "Perguntas.txt");
		long size = db.getTamArquivo();

		inserirPergunta(p);
		db.fechaDB();
		db = new Database("rw", "PerguntasIndex.txt");

		inserirIndex(raiz,intToByteArray((int)size),intToByteArray(p.getId()));
		db.fechaDB();
		return raiz;
	}
	
	private boolean inserirPergunta(Pergunta p){
		
		db.escreveObjeto(p);
		db.escreveChar('|');

		return true;
	}
	
	private No inserirIndex(No no, byte[] endereco, byte[] id){

		if(no == null){
			db.ponteiro(db.getTamArquivo());
			db.escreveChar(0);
			db.escreveBytes(endereco);
			db.escreveBytes(id);
			db.escreveInt(-1);
			db.escreveInt(-1);
			
		}else if(byteArrayToInt(no.id) < byteArrayToInt(id)){
	
			db.ponteiro(byteArrayToInt(no.dir)+1);	
			byte[] b = db.leBytes(17);

			no = new No(b);
			no = inserirIndex(no,endereco,id);
			
		}else if(byteArrayToInt(no.id) > byteArrayToInt(id)){
			
			
			db.ponteiro(byteArrayToInt(no.esq)+1);	
			byte[] b = db.leBytes(17);

			no = new No(b);
			no = inserirIndex(no,endereco,id);

			
			no = new No(b);
			no = inserirIndex(no,endereco,id);
		}else{
			System.out.println("Falha na inserção");
		}
			
			
		
		return no;
	}
	public static byte[] intToByteArray(int a)
	{
	    return new byte[] {
	        (byte) ((a >> 24) & 0xFF),
	        (byte) ((a >> 16) & 0xFF),   
	        (byte) ((a >> 8) & 0xFF),   
	        (byte) (a & 0xFF)
	    };
	}
	
	public static int byteArrayToInt(byte[] b) 
	{
	    return   b[3] & 0xFF |
	            (b[2] & 0xFF) << 8 |
	            (b[1] & 0xFF) << 16 |
	            (b[0] & 0xFF) << 24;
	}
	
	
}