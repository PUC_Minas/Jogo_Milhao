package br.pucminas.aed3;

import java.io.File;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;


public class Pergunta implements Serializable{
	private int id;
	private String pergunta;
	private String alternativaA;
	private String alternativaB;
	private String alternativaC;
	private String alternativaD;
	private int resposta;
	private int dificuldade;
	private int tamRegistro;

	
	public Pergunta(String pergunta, String alternativaA, String alternativaB,String alternativaC, String alternativaD, int resposta, int dificuldade) {
		this.pergunta = pergunta;
		this.alternativaA = alternativaA;
		this.alternativaB = alternativaB;
		this.alternativaC = alternativaC;
		this.alternativaD = alternativaD;
		this.resposta = resposta;
		this.dificuldade = dificuldade;
	}
	
	public int getTamRegistro() throws UnsupportedEncodingException{
		tamRegistro = 0;
		File f = new File("Perguntas.txt");
		long size = f.length();
		byte[] utf8Bytes = pergunta.getBytes("UTF-8");	
		tamRegistro += utf8Bytes.length;	
		utf8Bytes = alternativaA.getBytes("UTF-8");
		tamRegistro += utf8Bytes.length;	
		utf8Bytes = alternativaB.getBytes("UTF-8");
		tamRegistro += utf8Bytes.length;	
		utf8Bytes = alternativaC.getBytes("UTF-8");
		tamRegistro += utf8Bytes.length;	
		utf8Bytes = alternativaD.getBytes("UTF-8");
		tamRegistro += utf8Bytes.length;
		utf8Bytes = String.valueOf(resposta).getBytes("UTF-8");
		tamRegistro += utf8Bytes.length;
		utf8Bytes = String.valueOf(dificuldade).getBytes("UTF-8");
		tamRegistro += utf8Bytes.length;
		utf8Bytes = String.valueOf(id).getBytes("UTF-8");
		tamRegistro += utf8Bytes.length;
		tamRegistro = (int) (size-(tamRegistro+7));//Tamanho registro + numero de delimitadores
		return tamRegistro; 
	}
	
	public String getAlternativaA() {
		return alternativaA;
	}
	public void setAlternativaA(String alternativaA) {
		this.alternativaA = alternativaA;
	}
	public String getAlternativaB() {
		return alternativaB;
	}
	public void setAlternativaB(String alternativaB) {
		this.alternativaB = alternativaB;
	}
	public String getAlternativaC() {
		return alternativaC;
	}
	public void setAlternativaC(String alternativaC) {
		this.alternativaC = alternativaC;
	}
	public String getAlternativaD() {
		return alternativaD;
	}
	public void setAlternativaD(String alternativaD) {
		this.alternativaD = alternativaD;
	}
	public int getResposta() {
		return resposta;
	}
	public void setResposta(int resposta) {
		this.resposta = resposta;
	}

	public int getDificuldade() {
		return dificuldade;
	}

	public void setDificuldade(int dificuldade) {
		this.dificuldade = dificuldade;
	}

	public String getPergunta() {
		return pergunta;
	}

	public void setPergunta(String pergunta) {
		this.pergunta = pergunta;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Pergunta clone(){
		Pergunta pergunta = new Pergunta(this.pergunta, alternativaA, alternativaB, alternativaC, alternativaD, resposta, dificuldade);
		return pergunta;
		
	}

}
