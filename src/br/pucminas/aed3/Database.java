package br.pucminas.aed3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;

public class Database {
	private RandomAccessFile raf;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private FileInputStream fis;
	private FileOutputStream fos;
	String fileName;
	File file;
	String mode;
	
	public Database(String mode, String fileName){
		this.fileName = fileName;
		this.mode = mode;
		file = new File(fileName);
		if(fileName.equals("PerguntasIndex.txt")){
			try {
				raf = new RandomAccessFile(fileName, mode);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			if(mode.equals("r")){
				try {
					fis = new FileInputStream(fileName);
					ois = new ObjectInputStream(fis);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				try {
					fis = new FileInputStream(fileName);
					ois = new ObjectInputStream(fis);
					fos = new FileOutputStream(fileName);
					oos = new ObjectOutputStream(fos);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}
	
	public static byte[] intToByteArray(int a)
	{
	    return new byte[] {
	        (byte) ((a >> 24) & 0xFF),
	        (byte) ((a >> 16) & 0xFF),   
	        (byte) ((a >> 8) & 0xFF),   
	        (byte) (a & 0xFF)
	    };
	}
	
	public static int byteArrayToInt(byte[] b) 
	{
	    return   b[3] & 0xFF |
	            (b[2] & 0xFF) << 8 |
	            (b[1] & 0xFF) << 16 |
	            (b[0] & 0xFF) << 24;
	}
	
	public boolean escreveObjeto(Object o){
		try {
			oos.writeObject(o);
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public Object leObjeto(){
		try {
			return ois.readObject();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public byte[] leBytes(int n){
		byte[] b = new byte[n];
		try {
			 raf.read(b);

			 return b.clone();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	
	public boolean escreveBytes(byte[] b){
		try {
			raf.write(b);
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
	}
	
	public byte leByte(){
		try {
			return raf.readByte();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
	}
	
	public boolean escreveByte(byte b){
		try {
			raf.writeByte(b);
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public int le(){
		try {
			return raf.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
	}
	
	public boolean escreveByte(int i){
		try {
			raf.write(i);
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean escreveChar(int c){
		try {
			oos.writeChar(c);
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean escreveInt(int i){
		try {
			raf.writeInt(i);
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean ponteiro(long i){
		try {
			raf.seek(i);
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public long getTamArquivo(){
		return file.length();
	}
	
	public boolean fechaDB(){
		if(fileName.equals("PerguntasIndex.txt")){
				try {
					raf.close();
					return true;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return false;
				}
		}else{
			if(mode.equals("r")){
				
				try {
					ois.close();
					fis.close();
					
					return true;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return false;
				}
				
			}else{
				try {
					ois.close();
					fis.close();
					oos.close();
					fos.close();
					return true;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return false;
				}
			}
		}
		
	}
	

}
