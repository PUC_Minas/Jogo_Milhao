package br.pucminas.aed3;

public class Jogador {
	private int id;
	private String nome;
	private int pontuacao;
	private boolean lapide;

	
	public Jogador(String nome, int id) {
		this.nome = nome;
		pontuacao = 0;
		lapide = false;
		this.setId(id);
	}
	
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getPontuacao() {
		return pontuacao;
	}
	public void setPontuacao(int pontuacao) {
		this.pontuacao = pontuacao;
	}

	public boolean isLapide() {
		return lapide;
	}

	public void setLapide(boolean lapide) {
		this.lapide = lapide;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	

}
